from plantas import Plantas
from children import Children
class Rabano(Plantas):
    def __init__(self, owner):
        super().__init__(owner)
        self._type = "Rabano"

    @property
    def type(self):
        return self._type