from plantas import Plantas
from children import Children
class Trebol(Plantas):
    def __init__(self, owner):
        super().__init__(owner)
        self._type = "Trebol"

    @property
    def type(self):
        return self._type