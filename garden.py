from children import Children
from violeta import Violeta
from rabano import Rabano
from hierba import Hierba
from trebol import Trebol

import random
#creo una lista con los nombres en orden alfabetico
class Garden():
    def __init__(self):
        self._children = ["Alicia", "Andres", "Belen", "David", "Eva", "Jose", "Larry",
                            "Lucia", "Marit", "Pepito", "Rocio", "Sergio"]

    @property
    def children(self):
        return self._children

    @children.setter
    def children(self, child):
        if isinstance(child, Children):
            self._children.append(child)
        else:
            print("tipo de dato no valido")

    def crear_estudiantes(self):
        for i in range(len(self.children)):
            self.children[i] = Children(self.children[i])

    def dar_plantas(self):
        for i in self.children:
            for x in range(4):
                eleccion = random.randint(1,4)
                if eleccion == 1:
                    i.plantas = Violeta(i)
                elif eleccion == 2:
                    i.plantas = Rabano(i)
                elif eleccion == 3:
                    i.plantas = Hierba(i)
                elif eleccion == 4:
                    i.plantas = Trebol(i)
                
    def mostrar_mesa(self):
        plantas_fila1 = []
        plantas_fila2 = []
        for i in self.children:
            plantas_fila1.append(i.plantas[0].type[0])
            plantas_fila1.append(i.plantas[1].type[0])
            plantas_fila2.append(i.plantas[2].type[0])
            plantas_fila2.append(i.plantas[3].type[0])
        print("[ventana][ventana][ventana]")
        print("".join(plantas_fila1))
        print("".join(plantas_fila2))

    def mostrar_planta_estudiante(self, child):
        plantas_fila1 = []
        plantas_fila2 = []
        for i in self.children:
            for j in range(len(i.plantas)):
                if j <= 1:
                    if i.plantas[j].owner is child:
                        plantas_fila1.append(i.plantas[j].type[0])
                    else:
                        plantas_fila1.append("°")
                else:
                    if i.plantas[j].owner is child:
                        plantas_fila2.append(i.plantas[j].type[0])
                    else:
                        plantas_fila2.append("°")
        print("[ventana][ventana][ventana]")
        print("".join(plantas_fila1))
        print("".join(plantas_fila2))

    def menu(self):
        print("ingrese un numero segun el niño que quiera ver: ")
        for i in range(len(self.children)):
            print(f"{i+1}.{self.children[i].nombre}")
        print("0.salir")
        while True:
            eleccion = int(input("ingrese el numero: "))
            if 1 <= eleccion <= 12:
                self.mostrar_planta_estudiante(self.children[eleccion-1])
            else:
                break







garden = Garden()
garden.crear_estudiantes()
garden.dar_plantas()
garden.mostrar_mesa()
garden.menu()
