from plantas import Plantas

class Children():
    def __init__(self, nombre):
        self._nombre = nombre
        self._plantas = []
        
    @property
    def nombre(self):
        return self._nombre

    @property
    def plantas(self):
        return self._plantas

    @plantas.setter
    def plantas(self, planta):
        if isinstance(planta, Plantas):
            self._plantas.append(planta)
        else:
            print("tipo de dato no valido")

