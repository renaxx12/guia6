from plantas import Plantas
from children import Children
class Hierba(Plantas):
    def __init__(self, owner):
        super().__init__(owner)
        self._type = "Hierba"

    @property
    def type(self):
        return self._type